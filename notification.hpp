#ifndef _NOTIFICATION_H
#define _NOTIFICATION_H
#include <iostream>
#include <vector>
#include <algorithm>
#include "user.hpp"
using namespace std;

class User;

class Notification
{
	public:
		Notification(User* _user , string _activity , int _id);
		void see_notification();
	private:
		User* user;
		string activity;
		int id;
};

#endif