#ifndef HANDLER
#define HANDLER

#include <iostream>
#include <string>
#include "../server/server.hpp"
#include "../app.hpp"


class LoginHandler : public RequestHandler
{
public:
	LoginHandler(App* _app);
	Response* callback(Request* req);
private:
	App* app;
};


class SignupHandler : public RequestHandler
{
public:
	SignupHandler(App* _app);
	Response* callback(Request* req);
private:
	App* app;
};

class LogoutHandler : public RequestHandler
{
public:
	LogoutHandler(App* _app);
	Response* callback(Request* req);
private:
	App* app;
};

class HomeHandler : public RequestHandler
{
public:
	HomeHandler(App* _app);
	Response* callback(Request* req);
private:
	App* app;
};

#endif

