#include "handler.hpp"
using namespace std;

LoginHandler::LoginHandler(App* _app)
{
	app = _app;
}
Response* LoginHandler::callback(Request* req)
{
	vector<string> info;
	string user = req->getBodyParam("username");
	string pass = req->getBodyParam("password");
	info.push_back("login");
	info.push_back(user);
	info.push_back(pass);
	if(app->login(info))
		return Response::redirect("/");
	else
		return Response::redirect("/404");
}

SignupHandler::SignupHandler(App* _app)
{
	app = _app;
}

Response* SignupHandler::callback(Request* req)
{
	vector<string> info;
	string user = req->getBodyParam("username");
	string pass = req->getBodyParam("password");
	string display_name = req->getBodyParam("displayname");
	info.push_back("");
	info.push_back(user);
	info.push_back(pass);
	info.push_back(display_name);
	if(app->signup(info))
		return Response::redirect("/login");
	else
		return Response::redirect("/404");
}

LogoutHandler::LogoutHandler(App* _app)
{
	app = _app;
}

Response* LogoutHandler::callback(Request* req)
{
	app->logout();
	return Response::redirect("/login");
}

HomeHandler::HomeHandler(App* _app)
{
	app = _app;
}

Response* HomeHandler::callback(Request* req)
{
	if(app->get_login_user() == NULL)
		return Response::redirect("/login");
	vector<string> jeek_text, jeek_text2;
	jeek_text.push_back("salam only half an hour!");
	jeek_text2.push_back("minus 5 minutes??");
	app->new_jeek(jeek_text, true);
	app->new_jeek(jeek_text2, true);
	vector<Jeek*> jeeks = app->find_user_jeeks(app->get_login_user()->get_user_name());

	Response *res = new Response;
	res->setHeader("Content-Type", "text/html");
	string body;
	body += "<!DOCTYPE html>";
	body += "<html>";
	body += "<body style=\"text-align: center;\">";
	body += "<div> Hi " + app->get_login_user()->get_display_name();
	body += "<br><a href=\"/logout\">logout</a>";
	body += "</div>";
	body += "<h1>AP HTTP</h1>";
	body += "<p>";
	for (int i = 0; i < jeeks.size(); ++i)
	{
		
		body += "@" + app->get_login_user()->get_user_name()+":";
		body += jeeks[i]->gettext();
		body += "<br><br>";
	}
	body += "</p>";
	body += "</body>";
	body += "</html>";
	res->setBody(body);
	return res;
}
