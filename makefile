CC=g++
STD=-std=c++11 -Wall -pedantic
CF=$(STD)
BUILD_DIR=build

all: $(BUILD_DIR) server.out

$(BUILD_DIR):
	mkdir -p $(BUILD_DIR)

$(BUILD_DIR)/response.o: utils/response.cpp utils/response.hpp utils/include.hpp
	$(CC) $(CF) -c utils/response.cpp -o $(BUILD_DIR)/response.o

$(BUILD_DIR)/request.o: utils/request.cpp utils/request.hpp utils/include.hpp utils/utilities.hpp
	$(CC) $(CF) -c utils/request.cpp -o $(BUILD_DIR)/request.o

$(BUILD_DIR)/utilities.o: utils/utilities.cpp utils/utilities.hpp
	$(CC) $(CF) -c utils/utilities.cpp -o $(BUILD_DIR)/utilities.o

$(BUILD_DIR)/server.o: server/server.cpp server/server.hpp server/route.hpp utils/utilities.hpp utils/response.hpp utils/request.hpp utils/include.hpp
	$(CC) $(CF) -c server/server.cpp -o $(BUILD_DIR)/server.o

$(BUILD_DIR)/route.o: server/route.cpp server/route.hpp utils/utilities.hpp utils/response.hpp utils/request.hpp utils/include.hpp
	$(CC) $(CF) -c server/route.cpp -o $(BUILD_DIR)/route.o

$(BUILD_DIR)/server_main.o: server/main.cpp server/server.hpp utils/utilities.hpp utils/response.hpp utils/request.hpp utils/include.hpp
	$(CC) $(CF) -c server/main.cpp -o $(BUILD_DIR)/server_main.o

$(BUILD_DIR)/app.o: app.cpp $(BUILD_DIR)/jeek.o $(BUILD_DIR)/notification.o
	g++ -c app.cpp -o $(BUILD_DIR)/app.o

$(BUILD_DIR)/user.o: user.cpp $(BUILD_DIR)/notification.o
	g++ -c user.cpp -o $(BUILD_DIR)/user.o

$(BUILD_DIR)/share.o: share.cpp $(BUILD_DIR)/user.o
	g++ -c share.cpp -o $(BUILD_DIR)/share.o

$(BUILD_DIR)/reply.o: reply.cpp $(BUILD_DIR)/share.o
	g++ -c reply.cpp -o $(BUILD_DIR)/reply.o

$(BUILD_DIR)/notification.o: notification.cpp $(BUILD_DIR)/user.o
	g++ -c notification.cpp -o $(BUILD_DIR)/notification.o

$(BUILD_DIR)/comment.o: comment.cpp $(BUILD_DIR)/share.o
	g++ -c comment.cpp -o $(BUILD_DIR)/comment.o

$(BUILD_DIR)/jeek.o: jeek.cpp $(BUILD_DIR)/share.o $(BUILD_DIR)/notification.o
	g++ -c jeek.cpp -o $(BUILD_DIR)/jeek.o

$(BUILD_DIR)/handler.o: request/handler.cpp $(BUILD_DIR)/app.o $(BUILD_DIR)/user.o $(BUILD_DIR)/share.o 
	g++ -std=c++11 -o $(BUILD_DIR)/handler.o -c request/handler.cpp

server.out: $(BUILD_DIR)/response.o $(BUILD_DIR)/request.o $(BUILD_DIR)/utilities.o $(BUILD_DIR)/server.o $(BUILD_DIR)/route.o $(BUILD_DIR)/server_main.o $(BUILD_DIR)/app.o $(BUILD_DIR)/user.o $(BUILD_DIR)/share.o $(BUILD_DIR)/reply.o $(BUILD_DIR)/comment.o $(BUILD_DIR)/notification.o $(BUILD_DIR)/handler.o $(BUILD_DIR)/jeek.o
	$(CC) $(CF) $(BUILD_DIR)/response.o $(BUILD_DIR)/request.o $(BUILD_DIR)/utilities.o $(BUILD_DIR)/server.o $(BUILD_DIR)/route.o $(BUILD_DIR)/server_main.o $(BUILD_DIR)/app.o $(BUILD_DIR)/user.o $(BUILD_DIR)/share.o $(BUILD_DIR)/reply.o $(BUILD_DIR)/comment.o $(BUILD_DIR)/notification.o $(BUILD_DIR)/handler.o $(BUILD_DIR)/jeek.o -o server.out

.PHONY: clean
clean:
	rm -rf $(BUILD_DIR) *.o *.out &> /dev/null
