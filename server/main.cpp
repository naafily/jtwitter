#include <cstdlib>
#include <ctime> 
#include <iostream>
#include "server.hpp"
#include "../request/handler.hpp"
#include "../app.hpp"
using namespace std;


int main(int argc, char **argv) 
{
	App* app = new App();
	srand(time(NULL));
	Server server(argc > 1 ? atoi(argv[1]) : 55000, "static/404.html");
	server.get("/login", new ShowPage("static/logincss.html"));
	server.post("/login", new LoginHandler(app));
	server.get("/signup", new ShowPage("static/signup.html"));
	server.get("/", new HomeHandler(app));
	server.get("/home.png", new ShowImage("static/home.png"));
	server.post("/signup", new SignupHandler(app));
	server.get("/logout", new LogoutHandler(app));
	server.run();  
}
