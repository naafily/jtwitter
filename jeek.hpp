#ifndef _JEEK_H
#define _JEEK_H
#include <iostream>
#include <vector>
#include <algorithm>
#include "share.hpp"
#include "notification.hpp"

using namespace std;
class Jeek: public Share
{
	public:
		Jeek(vector<string> _text, User* _user, int _id);
		void publish();
		void is_rejeeked();
		void mention(User*);
		void tag(string);
		void print(vector<int>);
		void print_user_jeeks();
		bool has_hashtag(string );
		bool has_user_name(string);
		void like(User*);
		void dislike(User*);
		vector<string> gettag(){ return tags; }
		vector<User*> getmention(){ return who_mentions; }
		string gettext();
	private:
		vector <User*> who_mentions;
		vector <string> tags;
		int rejeeks_num = 0;
		vector<User*> likers;
};
#endif