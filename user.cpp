#include <iostream>
#include <vector>
#include <algorithm>
#include "user.hpp"
using namespace std;

User::User(string _user_name, string _display_name, string _password)
{
	start_pos = 0;
	user_name = _user_name;
	display_name = _display_name;
	password = _password;
}
void User::follow(User* user)
{
	int flag = 0;
	for(int i = 0; i < followers.size(); i++)
	{
		if(followers[i] == user)
			flag = 1;
	}
	if(flag == 0)
		followers.push_back(user);
}
void User::unfollow(User* user)
{
	for(int i = 0; i < followers.size(); i++)
	{
		if(followers[i] == user)
			followers.erase(followers.begin() + i);
	}
}
void User::see_notification()
{
	for(int i = start_pos; i < notifications.size(); i++)
		notifications[i]->see_notification();
	start_pos = notifications.size();
}
void User::notification(Notification* n)
{
	notifications.push_back(n);
}