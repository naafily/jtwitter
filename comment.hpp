#ifndef _COMMENT_H
#define _COMMENT_H
#include <iostream>
#include <vector>
#include <algorithm>
#include "share.hpp"
using namespace std;
class Comment : public Share
{
	public:
		Comment(vector<string> _text, User* _user, int _id, int _jeek_id);
		int get_jeek_id(){return jeek_id;}
		void print(vector<int>);
	private:
		int jeek_id;
};
#endif