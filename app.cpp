#include "app.hpp"


App::App()
{
	jeek_to_be_added = NULL;
	login_user = NULL;
	end_jeek_process_flag = 0;
}
void App::split_input()
{
	string line;
	while(getline(cin, line))
	{
		vector<string> input;
		stringstream ss(line);
		while(ss)
		{
			string word;
			ss >> word;
			input.push_back(word);
		}
		if(end_jeek_process_flag == 0)
			check_command(input);
		else 
			jeek_process(input);
	}
	
}
void App::new_jeek(vector<string> text, bool test)
{  
	int id = publish_jeeks.size();
	jeek_to_be_added = new Jeek(text, login_user, id);
	if(test)
		publish_jeeks.push_back(jeek_to_be_added);
}
void App::jeek_process(vector<string> input)
{   
	if(input[0] == "text")
	{
		if(jeek_to_be_added == NULL)
			new_jeek(input);
		else 
			jeek_to_be_added->texts(input);
	}

	if(input[0] == "mention")
	{
		int jeek_id = jeek_to_be_added->get_id();
		Notification * n = new Notification(login_user, "mentioned", jeek_id);
		User* user = find_by_user_name(input[1]);
		jeek_to_be_added->mention(user);
		user->notification(n);
	}
	if(input[0] == "tag")
	{
		jeek_to_be_added->tag(input[1]);
		cout<<" your hashtag added "<<endl;
	}

	if(input[0] == "publish")
	{   
		int jeek_id = jeek_to_be_added->get_id();
		Notification * n = new Notification(login_user, "jeeked", jeek_id);
		publish_jeeks.push_back(jeek_to_be_added);
		end_jeek_process_flag = 0;
		jeek_to_be_added = NULL;
		cout << " you jeeked successfully!" << endl;
		for(int i = 0; i < login_user->getfollowers().size(); i++)
			login_user->getfollowers()[i]->notification(n);
	}
	if(input[0] == "abort")
	{
		end_jeek_process_flag = 0;
		jeek_to_be_added = NULL;
		cout << "aborted" << endl;
	}
}
int App::str_to_int(string str)
{
	int i;
	stringstream ss(str);
	ss >> i;
	return i;
}
void App::show_jeek(string str)
{
	Jeek* j = find_jeek(str);
	int id = j-> get_id();
	vector<int> num;
	for(int i = 0; i < comments.size(); i++)
	{
		if(comments[i]-> get_jeek_id() == id)
			num.push_back(comments[i]->get_id());
	}
	j->print(num);
}
Comment* App::find_comment(string str)
{
	int end = str.size() -1;
	string s = str.substr(COMMENTIDSIZE,end);
	int id = str_to_int(s);
	return comments[id];
}      
Jeek* App::find_jeek(string str)
{
	int end = str.size() - 1;
	string s = str.substr(JEEKIDSIZE, end);
	int id = str_to_int(s);
	return publish_jeeks[id];
}
void App::check_command(vector<string> input)
{
	if(input[0] == "signup")
		signup(input);
	else if(input[0] == "login")
		login(input);
	else if(input[0] == "logout")
		logout();
	else if(input[0] == "jeek")
	{
		if(login_user == NULL)
			throw BadInputExc("plz login");
		end_jeek_process_flag = 1;
	}
	else if(input[0] == "showJeek")
		show_jeek(input[1]);
	else if(input[0] == "search")
		which_search(input[1]);
	else if(input[0] == "comment")
		comment(input);
	else if(input[0] == "reply")
		reply(input);
	else if(input[0] == "rejeek")
		rejeek(input[1]);
	else if(input[0] == "showComment")
		show_comment(input[1]);
	else if(input[0] == "showReply")
		show_reply(input[1]);
	else if(input[0] == "like")
		like(input[1]);
	else if(input[0] == "dislike")
		dislike(input[1]);
	else if(input[0] == "follow")
		follow(input[1]);
	else if(input[0] == "unfollow")
		unfollow(input[1]);
	else if(input[0] == "notification")
		notifications();
	else 
		throw BadInputExc("Command not declared");

}
void App::notifications()
{
	login_user->see_notification();
}
void App::follow(string input)
{
	User* u = find_by_user_name(input);
	login_user -> follow(u);
}
void App::unfollow(string input)
{
	User* u = find_by_user_name(input);
	login_user -> unfollow(u);
}
void App::like(string input)
{
	Jeek *jeek = find_jeek(input);
	jeek -> like(login_user);
}
void App::dislike(string input)
{
	Jeek *jeek = find_jeek(input);
	jeek -> dislike(login_user);
}
Reply* App::find_reply(string str)
{
	int end = str.size() - 1;
	string s = str.substr(REPLYIDSIZE,end);
	int id = str_to_int(s);
	return replies[id];
}   
void App::show_reply(string input)
{
	Reply* r = find_reply(input);
	int id = r -> get_id();
	vector<int> num;
	if(input.substr(0,COMMENTIDSIZE) == "commentId")
	{
		for(int i = 0; i < replies.size(); i++)
		{
			if(replies[i]-> get_comment_id() == id)
				num.push_back(replies[i]->get_id());
		}
	}
	else if(input.substr(0,REPLYIDSIZE) == "replyId")
	{
		for(int i = 0; i < replies.size(); i++)
		{	
			if(replies[i]-> get_reply_id() == id)
				num.push_back(replies[i]->get_id());
		}
	}
	r->print(num);
}
void App::show_comment(string input)
{
	Comment* c = find_comment(input);
	int id = c -> get_id();
	vector<int> num;
	for(int i = 0; i < replies.size(); i++)
	{
		if(replies[i]-> get_comment_id() == id)
			num.push_back(replies[i]->get_id());
	}
	c->print(num);
}
void App::rejeek(string input)
{
	Jeek *jeek = find_jeek(input);
	jeek->is_rejeeked();
	vector<string> s;
	s.push_back("Rejeeked");
	Notification * n = new Notification(login_user, "rejeeked", jeek->get_id());
	for(int i = 1; i < jeek->get_text().size(); i++)
		s.push_back(jeek->get_text()[i]);
	new_jeek(s);
	publish_jeeks.push_back(jeek_to_be_added);
	end_jeek_process_flag = 0;
	jeek_to_be_added = NULL;
	jeek->get_author()->notification(n);
}
void App::reply(vector<string> input)
{
	int reply_id = -1;
	int comment_id = -1;
	int end = input[1].size() - 1;
	if("replyId" == input[1].substr(0,REPLYIDSIZE))	
		reply_id =str_to_int(input[1].substr(REPLYIDSIZE,end));
	else if("commentId" == input[1].substr(0,COMMENTIDSIZE))
		comment_id = str_to_int(input[1].substr(COMMENTIDSIZE,end));
	vector<string> text;
	for(int i = 2; i < input.size(); i++)
		text.push_back(input[i]);
	int id = replies.size();
	if((reply_id > -1 && reply_id < replies.size()) || (comment_id > -1 && comment_id < comments.size()))
	{
		Reply* reply = new Reply(text, login_user, id, comment_id, reply_id);
		replies.push_back(reply);
		cout << "you replied successfully" << endl;
		if(reply_id == -1 && comment_id != -1)
		{
			Notification *n = new Notification(login_user, "replyed", comment_id);
			comments[comment_id]->get_author()->notification(n);
		}  
	}
}
void App::comment(vector<string> input)
{	
	Jeek * j = find_jeek(input[1]);
	int end = input[1].size() - 1;
	int jeek_id = str_to_int(input[1].substr(JEEKIDSIZE,end));
	vector<string> text;
	for(int i = 2; i < input.size(); i++)
		text.push_back(input[i]);
	int id = comments.size();
	if(jeek_id > -1 && jeek_id < publish_jeeks.size())
	{
		Notification * n = new Notification(login_user, "commented", jeek_id);
		Comment* comment = new Comment(text, login_user, id, jeek_id);
		comments.push_back(comment);
		j->get_author()->notification(n);
	}
}
void App::which_search(string str)
{
	int end = str.size() - 1 ;
	string ch = str.substr(0, 1);
	string searched = str.substr(1, end);
	if(ch == ATSIGN)
		search_by_user_name(searched);
	else if(ch == SHARPSIGN)             
		search_by_hashtag(searched);
	else
		throw BadInputExc("your search is incorrect!");

}
vector<Jeek*> App::find_user_jeeks(string user_name)
{	
	vector<Jeek*> j;
	cout << publish_jeeks.size() << endl;

	for(int i = 0; i < publish_jeeks.size(); i++)
	{
		if(publish_jeeks[i]->has_user_name(user_name))
			j.push_back(publish_jeeks[i]);
	}
	return j;
}
vector<Jeek*> App::find_hashtag_jeeks(string hashtag)
{
	vector<Jeek*> j;
	for(int i = 0; i < publish_jeeks.size(); i++)
	{
		if(publish_jeeks[i]->has_hashtag(hashtag))
			j.push_back(publish_jeeks[i]);
	}
	return j;
}
void App::search_by_hashtag(string hashtag)
{
	vector<Jeek*> j;
	j = find_hashtag_jeeks(hashtag);
	print_user_jeeks(j);
}
void App::search_by_user_name(string user_name)
{
	vector<Jeek*> j;
	j = find_user_jeeks(user_name);
	print_user_jeeks(j);
}
void App::print_user_jeeks(vector<Jeek*> user_jeeks)
{
	for(int i = 0; i < user_jeeks.size(); i++)
		user_jeeks[i]->print_user_jeeks();
}
bool App::check_duplicate_user(string user_name)
{
	return(find_by_user_name(user_name) != NULL);
}
bool App::signup(vector<string> input)
{
	if(!check_duplicate_user(input[1])) {
		users.push_back(new User(input[1], input[2], input[3]));
		return true;
	}
	else
		return false;
}
bool App::check_existance_user(string user_name, string password)
{
	for(int i = 0; i < users.size(); i++)
	{
		if(users[i]->get_user_name() == user_name && users[i]->get_pass() == password)
			return true;
	}
	return false; 
}
User* App::find_by_user_name(string user_name)
{	
	for(int i = 0; i < users.size(); i++)
	{
		if(users[i]->get_user_name() == user_name)
			return users[i];
	}
}
bool App::login(vector<string> input)
{
	if(login_user == NULL)
	{
		if(check_existance_user(input[1], input[2])){
			login_user = find_by_user_name(input[1]);
			return true;
		}
		return false;
	}
}
void App::logout()
{
	if(login_user != NULL)
		login_user = NULL;
}