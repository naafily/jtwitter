#ifndef _APP_H
#define _APP_H
#include <string>
#include <sstream>
#include <stdlib.h> 
#include <fstream>
#include <iostream>
#include <vector>
#include <algorithm>
#include "jeek.hpp"
#include "comment.hpp"
#include "reply.hpp"
using namespace std;
const int COMMENTIDSIZE = 9;
const int JEEKIDSIZE = 6;
const int REPLYIDSIZE = 7;
const string ATSIGN = "@";
const string SHARPSIGN = "#";

class BadInputExc
{
	public:
		BadInputExc(string _txt = "") { txt = _txt; }
		string txt;
};

class App
{
	public:
		App();
		void split_input();
		void check_command(vector<string>);
		bool check_duplicate_user(string);
		bool signup(vector<string>);
		bool check_existance_user(string, string);
		bool login(vector<string>);
		void logout();
		void jeek_process(vector<string>);
		void new_jeek(vector<string>, bool test=false);
		void show_jeek(string);
		int str_to_int(string str);
		void print_user_jeeks(vector<Jeek*>);
		void which_search(string);
		void search_by_user_name(string);
		void search_by_hashtag(string);
		void comment(vector<string>);
		void reply(vector<string> input);
		void show_comment(string);
		void rejeek(string);
		void show_reply(string);
		void like(string);
		void dislike(string);
		void follow(string);
		void unfollow(string);
		void notifications();
		User* find_by_user_name(string);
		User* get_login_user(){ return login_user; }
		string extract_id(string);
		Jeek* find_jeek(string);
		string find_display_name(string);
		vector<Jeek*> find_user_jeeks(string);
		vector<Jeek*> find_hashtag_jeeks(string);
		Comment* find_comment(string );
		Reply* find_reply(string);
	private:
		vector<User*> users;
		vector<Jeek*> publish_jeeks;
		vector<Reply*> replies;
		vector<Comment*> comments;
		Jeek* jeek_to_be_added;
		User* login_user;
		int end_jeek_process_flag;
};
#endif