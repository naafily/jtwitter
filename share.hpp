#ifndef _SHARE_H
#define _SHARE_H
#include <iostream>
#include <vector>
#include <algorithm>
#include "user.hpp"
using namespace std;

#define TEXTSIZE 140

class BadJeek
{
	public:
		BadJeek(string _txt = "") { txt = _txt; }
		string txt;
};
class Share
{
	public:
		Share(vector<string> _text, User* _user, int _id);
		void texts(vector<string>);
		User* get_author(){ return author; }
		vector<string> get_text(){ return text; }
		int get_id(){return id;}
		void print_text();
		bool size_is_correct(vector<string> );
	protected:
		vector<string> text;
		User* author;
		int id;
};
#endif
