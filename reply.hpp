#ifndef _REPLY_H
#define _REPLY_H
#include <iostream>
#include <vector>
#include <algorithm>
#include "share.hpp"
using namespace std;
class Reply : public Share
{
	public:
		Reply(vector<string> _text, User* _user, int _id, int _comment_id, int _reply_id);
		int get_comment_id(){ return comment_id; }
		int get_reply_id(){ return reply_id; }
		void print(vector<int>);
	private:
		int comment_id;
		int reply_id;
};
#endif