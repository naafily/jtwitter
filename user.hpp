#ifndef _USER_H
#define _USER_H
#include <iostream>
#include <vector>
#include <algorithm>
#include "notification.hpp"
using namespace std;

class Notification;

class User
{
	public:
		User(string _user_name = "", string _display_name = "", string _password = "");
	    string get_user_name(){ return user_name; }
	    string get_pass(){ return password; }
	    string get_display_name(){ return display_name; }
	    void see_notification();
	    void notification(Notification*);
	    void follow(User*);
		void unfollow(User*);
		vector<User*> getfollowers(){return followers; }
	private:
		int start_pos;
		string user_name;
		string display_name;
		string password;
		vector<User*> followers;
		vector<User*> following;
		vector<Notification*> notifications;
};

#endif