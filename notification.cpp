#include <iostream>
#include <vector>
#include <algorithm>
#include "notification.hpp"
using namespace std;

Notification::Notification(User* _user = NULL, string _activity = "", int _id = -1)
{
	user  = _user;
	activity = _activity;
	id = _id;
}
void Notification::see_notification()
{
	cout << user->get_user_name()<<" "<< activity <<" "<< id <<endl;
}