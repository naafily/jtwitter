#include "app.hpp"

int main()
{
	try
	{
		App app;
		app.split_input();
	}
	catch(BadInputExc e)
	{
		cerr << e.txt << endl;
	}

}